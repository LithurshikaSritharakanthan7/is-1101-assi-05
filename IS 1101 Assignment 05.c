#include<stdio.h>

#define PerformanceCost 500
#define PerAttendee 3

//Functions
intNoOfAttendees(int Price);
intIncome(int Price);
intCost(int Price);
intProfit(int Price);

int NoOfAttendees(int Price){
return 120-(Price-15)/5*20;
}

int Income(int Price){
return NoOfAttendees(Price)*Price;
}

int Cost (int Price){
return NoOfAttendees(Price)*PerAttendee+PerformanceCost ;
}

int Profit(int Price){
return Income(Price)-Cost(Price);
}

int main(){
int Price;
printf("\nExpected profit for ticket prices :\n\n");
for(Price=5;Price<50;Price+=5)
{
printf("Ticket price =Rs.%d\t\t Profit=Rs%d\n",Price,Profit(Price));
printf("--------------------------------------------\n\n");
}
return (0);
}
